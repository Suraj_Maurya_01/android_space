import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class MyRegister extends StatefulWidget {
  const MyRegister({Key? key}) : super(key: key);

  @override
  State<MyRegister> createState() => _MyRegisterState();
}

class _MyRegisterState extends State<MyRegister> {
  String email = '', pass = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration:const BoxDecoration(
          image: DecorationImage(image: AssetImage('assets/back2.jpg'),
              fit: BoxFit.cover)
      ),
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
        ),
        backgroundColor: Colors.transparent,
        body: Stack(
          children:  [
            Container(
              padding: const EdgeInsets.only(top: 50,left: 30),
              child: const Text('Create\nAccount',
                style: TextStyle(
                    fontSize:40,
                    color: Colors.white,
                    fontWeight: FontWeight.w500
                ),
              ),
            ),

            SingleChildScrollView(
              child: Container(
                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.3,left: 20,right: 20),
                  child: Column(
                    children: [
                      TextField(
                        onChanged: (value){
                          email = value;
                        },
                        decoration: InputDecoration(
                            hintText: 'Enter Email',
                            fillColor: Colors.white70,
                            filled: true,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            focusedBorder: const OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.white70,
                                )
                            )
                        ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),

                      TextField(
                        obscureText: true,
                          onChanged: (value){
                            pass = value;
                          },
                        decoration: InputDecoration(
                            hintText: 'Enter Password',
                            fillColor: Colors.white70,
                            filled: true,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            )
                        ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text("Sign In",style: TextStyle(
                              fontSize: 35,
                              fontWeight: FontWeight.w500,
                              color: Colors.grey
                          ),
                          ),
                          CircleAvatar(
                            radius: 30,
                            backgroundColor: Colors.grey,
                            child: IconButton (
                                color: Colors.white70,
                                onPressed: () async{
                                  try {
                                    UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
                                        email: email,
                                        password: pass
                                    );
                                    if (!mounted) return;
                                    Navigator.pushNamed(context, 'login');

                                  } on FirebaseAuthException catch (e) {
                                    if (e.code == 'weak-password') {
                                      print('The password provided is too weak.');
                                    } else if (e.code == 'email-already-in-use') {
                                      print('The account already exists for that email.');
                                    }
                                  } catch (e) {
                                    print(e);
                                  }
                                },
                                icon: const Icon(Icons.arrow_forward)),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          TextButton(onPressed: (){

                          },
                            child: const Text("Create",style: TextStyle(
                                fontSize: 20,
                                color: Colors.blueAccent,
                                decoration: TextDecoration.underline,
                                fontWeight: FontWeight.w500
                            ),
                            ),
                          ),
                          TextButton(
                            onPressed: (){},
                            child: const Text("Forgot Password",style: TextStyle(
                                fontSize: 20,
                                color: Colors.blueAccent,
                                decoration: TextDecoration.underline,
                                fontWeight: FontWeight.w500
                            ),
                            ),
                          ),
                        ],
                      )
                    ],
                  )
              ),
            ),
          ],
        ),
      ),
    );
  }
}
