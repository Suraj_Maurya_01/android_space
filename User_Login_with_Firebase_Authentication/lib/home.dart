
import 'package:flutter/material.dart';

class MyHome extends StatelessWidget {
  const MyHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/back3.webp'),
            fit: BoxFit.cover
          )
        ),
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.pinkAccent,
            title: const Text('Home'),
            elevation: 0,
            centerTitle: true,
          ),
          backgroundColor: Colors.transparent,
          body: Stack(
            children: [
               Container(
                 padding: EdgeInsets.only(top:60,left: 30),
                child: const Text("Welcome\nBack",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 40,
                    fontWeight: FontWeight.w500
                  ),
                ),
              )
            ],
          ),
        )
      );
  }
}
