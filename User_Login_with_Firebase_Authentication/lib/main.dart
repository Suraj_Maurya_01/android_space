
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:user_signin/first.dart';
import 'package:user_signin/home.dart';
import 'package:user_signin/register.dart';
import 'login.dart';

late final FirebaseApp app;
late final FirebaseAuth auth;

void  main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(
      MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: 'first',
        routes: {
          'first': (context) => const MyFirstPage(),
          'login': (context) => const MyLogin(),
          'register': (context) => const MyRegister(),
          'home' : (context) => const MyHome()
        },
      ));
}