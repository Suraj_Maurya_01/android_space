import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

void main() => runApp(const MyLogin());

class MyLogin extends StatefulWidget {
  const MyLogin({Key? key}) : super(key: key);

  @override
  State<MyLogin> createState() => _MyLoginState();
}

class _MyLoginState extends State<MyLogin> {
  String email = '', pass = '';

  @override
  Widget build(BuildContext context) {

    return Container(
      decoration:const BoxDecoration(
          image: DecorationImage(image: AssetImage('assets/background.jpg'),
              fit: BoxFit.cover)
      ),
      child: Scaffold(

        backgroundColor: Colors.transparent,
        body: Stack(
          children:  [
            Container(
              padding: const EdgeInsets.only(top: 100,left: 30),
              child: const Text('Welcome\nBack',
                style: TextStyle(
                    fontSize:40,
                    color: Colors.white,
                    fontWeight: FontWeight.w500
                ),),
            ),

            SingleChildScrollView(
              child: Container(
                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.5,left: 20,right: 20),
                  child: Column(
                    children: [
                      TextField(
                        onChanged: (value){
                          email = value;
                          },
                        decoration: InputDecoration(
                            hintText: 'Enter Email',
                            fillColor: Colors.grey.shade100,
                            filled: true,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            )
                        ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      TextField(
                      onChanged: (value){
                        pass = value;
                        },
                        obscureText: true,
                        decoration: InputDecoration(
                        hintText: 'Enter Password',
                        fillColor: Colors.grey.shade100,
                        filled: true,
                        border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        )
                        ),
                        ),
                        const SizedBox(
                        height: 30,
                        ),
                        Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                        const Text("Sign In",style: TextStyle(
                        fontSize: 35,
                              fontWeight: FontWeight.w500,
                              color: Colors.blueAccent
                          ),
                          ),
                          CircleAvatar(
                            radius: 30,
                            backgroundColor: Colors.blueAccent,
                            child: IconButton(
                                color: Colors.white70,
                                onPressed: () async{
                                  try {
                                    UserCredential userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(
                                        email: email,
                                        password: pass
                                    );
                                    if (!mounted) return;
                                    Navigator.pushNamed(context, 'home');

                                  } on FirebaseAuthException catch (e) {
                                    if (e.code == 'user-not-found') {
                                      print('No user found for that email.');
                                    } else if (e.code == 'wrong-password') {
                                      print('Wrong password provided for that user.');
                                    }
                                  }
                                },
                                icon: const Icon(Icons.arrow_forward)),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          TextButton(onPressed: (){
                            Navigator.pushNamed(context, 'register');
                          },
                            child: const Text("Sign Up",style: TextStyle(
                                fontSize: 20,
                                color: Colors.blueAccent,
                                decoration: TextDecoration.underline,
                                fontWeight: FontWeight.w500
                            ),
                            ),
                          ),
                          TextButton(onPressed: (){},
                            child: const Text("Forgot Password",style: TextStyle(
                                fontSize: 20,
                                color: Colors.blueAccent,
                                decoration: TextDecoration.underline,
                                fontWeight: FontWeight.w500
                            ),
                            ),
                          ),
                        ],
                      )
                    ],
                  )
              ),
            ),
          ],
        ),
      ),
    );
  }
}
